require 'rake'
require 'bundler'

#
# Optimiser Developer Environment extensions
#
class OptimiserTask

    # Local development and deployment stuff
    namespace :dev do
      @environment = 'dev'

      desc "Start jetty locally."
      task :server do
        local_jetty_init()
      end

    end

  def local_jetty_init()
    maven_opts = "-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n -Xmx1024m"
    jrebel_jar = "#{ENV['JREBEL_HOME']}/jrebel.jar"
    if File.exist?(jrebel_jar)
      puts "Using JRebel"
      maven_opts = "#{maven_opts} -noverify -javaagent:#{jrebel_jar} -Drebel.velocity_plugin=true -Drebel.log4j-plugin=true -Drebel.jackson_plugin=true -XX:+UseParallelGC -XX:+CMSClassUnloadingEnabled"
    end
    ENV['MAVEN_OPTS'] = maven_opts
    exec("mvn jetty:run -f query-optimiser-web/pom.xml -DCONSTRETTO_TAGS=dev -Dnet.jawr.debug.on=true ")
  end
end

