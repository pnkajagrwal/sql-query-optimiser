<%@page contentType="text/html; charset=UTF-8" isErrorPage="true" %>
<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@ page import="java.net.SocketException" %>

An error has occurred. We apologize for the inconvenience.

<%
  Logger LOG = LoggerFactory.getLogger("app-container");

  if (exception instanceof Error) {
    LOG.error("Caught Error in JSP. Critical application failure.", exception);
  } else {
    if (exception instanceof SocketException && "Broken pipe".equals(exception.getMessage())) {
      LOG.error(exception.toString());
    } else {
      LOG.error("Caught unhandled exception in JSP. Showing general error page.", exception);
    }
  }
%>
