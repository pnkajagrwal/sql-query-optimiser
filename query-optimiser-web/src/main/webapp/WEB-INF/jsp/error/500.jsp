<%@page contentType="text/html; charset=UTF-8" isErrorPage="true" %>
<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>

An internal error has occurred. Please try again and contact us if the problem persists.

<%
  Logger LOG = LoggerFactory.getLogger("app-container");

  String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
  Throwable t = (Throwable) request.getAttribute("javax.servlet.error.exception");

  if (requestUri != null) {
    LOG.error("Mapping 500 to general error page. Requested URI was " + requestUri, t);
  } else {
    LOG.error("Mapping 500 to general error page.", t);
  }
%>
