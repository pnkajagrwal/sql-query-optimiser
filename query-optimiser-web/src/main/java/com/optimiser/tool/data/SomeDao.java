package com.optimiser.tool.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class SomeDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    private SomeDao(DataSource ds) {
        jdbcTemplate = new JdbcTemplate(ds);
    }

    public int getNumberOfRows() {
        return jdbcTemplate.queryForObject("select count(*) from test_table", Integer.class);
    }
}
