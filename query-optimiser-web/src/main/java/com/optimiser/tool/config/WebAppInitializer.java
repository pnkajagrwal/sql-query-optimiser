package com.optimiser.tool.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;


import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import java.util.Set;

@WebAppConfiguration
public class WebAppInitializer implements WebApplicationInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(WebAppInitializer.class);

    @Override
    public void onStartup(ServletContext container) {
        container.setInitParameter("org.eclipse.jetty.servlet.SessionCookie", "app_xyz-sessionid");
        container.setInitParameter("org.eclipse.jetty.servlet.SessionIdPathParameterName", "none");

        AnnotationConfigWebApplicationContext rootContext = initRootContext(container);
        addListeners(container, rootContext);
        initDispatcher(container, rootContext);
    }

    private AnnotationConfigWebApplicationContext initRootContext(ServletContext container) {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(ApplicationConfig.class);
        rootContext.register(DatabaseConfig.class);
        rootContext.register(WebConfig.class);
        rootContext.setServletContext(container);
        // Since we registering instead of passing it to the constructor
        rootContext.refresh();
        return rootContext;
    }

    private void addListeners(ServletContext container, AnnotationConfigWebApplicationContext rootContext) {
        container.addListener(new ContextLoaderListener(rootContext));
        container.addListener(new RequestContextListener());
    }

    private void initDispatcher(ServletContext container, AnnotationConfigWebApplicationContext rootContext) {
        DispatcherServlet servlet = new DispatcherServlet(rootContext);
        ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", servlet);
        dispatcher.setLoadOnStartup(1);

        Set<String> mappingConflicts = dispatcher.addMapping("/");
        mappingConflicts.stream().forEach(LOG::warn);
    }

}