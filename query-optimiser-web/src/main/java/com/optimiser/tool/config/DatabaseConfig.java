package com.optimiser.tool.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.constretto.ConstrettoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.beans.PropertyVetoException;

@Configuration
public class DatabaseConfig {

    @Primary
    @Bean(destroyMethod = "close")
    public ComboPooledDataSource dataSource(ConstrettoConfiguration config) throws PropertyVetoException {
        DatabaseConfiguration dbParams = databaseConfig(config);
        ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass(dbParams.driverClassName);
        ds.setJdbcUrl(dbParams.url);
        ds.setUser(dbParams.username);
        ds.setPassword(dbParams.password);
        ds.setCheckoutTimeout(60000);
        ds.setMinPoolSize(1);
        ds.setMaxPoolSize(10);
        ds.setMaxStatements(200);
        ds.setIdleConnectionTestPeriod(1800);
        return ds;
    }

    private DatabaseConfiguration databaseConfig(ConstrettoConfiguration config) {
        return new DatabaseConfiguration(
            config.evaluateToString("database.driverClassName"),
            config.evaluateToString("database.url"),
            config.evaluateToString("database.username"),
            config.evaluateToString("database.password")
        );
    }

    private class DatabaseConfiguration {
        public final String driverClassName, url, username, password;

        private DatabaseConfiguration(String driverClassName, String url, String username, String password) {
            this.driverClassName = driverClassName;
            this.url = url;
            this.username = username;
            this.password = password;
        }
    }

}
