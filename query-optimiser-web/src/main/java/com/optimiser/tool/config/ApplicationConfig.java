package com.optimiser.tool.config;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.constretto.ConstrettoBuilder;
import org.constretto.ConstrettoConfiguration;
import org.constretto.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.optimiser" } )
public class ApplicationConfig {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationConfig.class);

    @Bean
    public static ConstrettoConfiguration constrettoConfiguration() {
        LOG.info("Initializing Constretto Configuration...");

        ConstrettoConfiguration configuration = new ConstrettoBuilder(true)
            .createPropertiesStore()
            .addResource(Resource.create("classpath:environment.properties"))
            .addResource(Resource.create("classpath:database.properties"))
            .done()
            .createSystemPropertiesStore()
            .getConfiguration();

        initAppLogLevel(configuration);
        return configuration;
    }

    private static void initAppLogLevel(ConstrettoConfiguration configuration) {
        Level level = Level.toLevel(configuration.evaluateToString("LogLevel"));
        LOG.info("Setting app log level to: {}", level);
        LogManager.getLogger("com.optimiser").setLevel(level);
    }

}
