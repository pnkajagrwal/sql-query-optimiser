package com.optimiser.tool.http;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

public class HttpClientFactory {

    public static HttpClient createHttpClient() {
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                         .register("http", PlainConnectionSocketFactory.getSocketFactory())
                         .register("https", SSLConnectionSocketFactory.getSocketFactory())
                        .build();

        return createHttpClient(registry);
    }

    public static HttpClient createUnsafeHttpClient() {
        try {
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", new SSLConnectionSocketFactory(createUnsafeHttpsConnections()))
                .build();

            return createHttpClient(registry);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new IllegalStateException("Failed when creating unsafe https client.", e);
        }
    }

    private static HttpClient createHttpClient(Registry<ConnectionSocketFactory> registry) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        // Increase default max connection per route to 25
        connectionManager.setDefaultMaxPerRoute(25);
        // Increase max total connection to 100
        connectionManager.setMaxTotal(100);

        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setConnectionManager(connectionManager);

        RequestConfig.Builder reqBuilder = RequestConfig.custom();
        // Turn off "Expect: 100-continue which can cause delays"
        reqBuilder.setExpectContinueEnabled(false);
        // Timeout in milliseconds until a connection is established in milliseconds
        reqBuilder.setConnectionRequestTimeout(10000);
        reqBuilder.setConnectTimeout(10000);
        // SO_TIMEOUT: maximum period inactivity between two consecutive data packets in milliseconds
        reqBuilder.setSocketTimeout(20000);
        // Getting an I/O error when executing a request over a connection that has been closed at the server side
        reqBuilder.setStaleConnectionCheckEnabled(true);
        reqBuilder.setRedirectsEnabled(true);
        reqBuilder.setRelativeRedirectsAllowed(true);

        httpClientBuilder.setDefaultRequestConfig(reqBuilder.build());

        // UserAgent
        httpClientBuilder.setUserAgent("Apache HttpClient");

        int retryCount = 3;
        DefaultHttpRequestRetryHandler retryThreeTimes = new DefaultHttpRequestRetryHandler(retryCount, true);
        httpClientBuilder.setRetryHandler(retryThreeTimes);

        return httpClientBuilder.build();
    }

    private static SSLContext createUnsafeHttpsConnections() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContext.getInstance("SSL");

        // set up a TrustManager that trusts everything
        sslContext.init(null, new TrustManager[] {
            new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        }, new SecureRandom());

        return sslContext;
    }

}
