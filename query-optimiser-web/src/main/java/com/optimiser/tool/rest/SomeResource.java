package com.optimiser.tool.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SomeResource {

    private static final Logger LOG = LoggerFactory.getLogger(SomeResource.class);

    @RequestMapping("/resource")
    public String getIt() {
        try {
            return "Success";
        }
        catch (Exception e) {
            LOG.error("Failed test", e);
            return null;
        }
    }
}
