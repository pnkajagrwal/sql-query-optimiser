
package com.optimiser.tool.web;

import com.optimiser.tool.data.SomeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SomeController {

    private final SomeDao dao;

    @Autowired
    public SomeController(SomeDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/controller")
    public String getIt(ModelMap model) {
        model.addAttribute("numberOfRows", dao.getNumberOfRows());
        return "controller";
    }
}
