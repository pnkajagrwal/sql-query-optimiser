package com.optimiser.tool.web;

import com.optimiser.tool.data.SomeDao;
import org.junit.Before;
import org.junit.Test;
import org.springframework.ui.ExtendedModelMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SomeControllerTest {

    private SomeController controller;

    @Before
    public void setUp() throws Exception {
        SomeDao someDao = mock(SomeDao.class);
        when(someDao.getNumberOfRows()).thenReturn(1);

        controller = new SomeController(someDao);
    }

    @Test
    public void should_return_something() throws Exception {
        ExtendedModelMap model = new ExtendedModelMap();

        String view = controller.getIt(model);

        assertThat(model.get("numberOfRows")).isEqualTo(1);
        assertThat(view).isEqualTo("controller");
    }

}