package com.optimiser.tool.rest;

import com.optimiser.tool.config.ApplicationConfig;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
public class SomeResourceIntegrationTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private SomeResource someResource;

    @Test
    public void should_test() throws Exception {
        String someResourceIt = someResource.getIt();
        assertThat(someResourceIt).isEqualTo("Success");
    }

}