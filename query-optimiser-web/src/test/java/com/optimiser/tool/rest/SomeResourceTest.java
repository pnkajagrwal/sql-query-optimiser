package com.optimiser.tool.rest;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SomeResourceTest {

    private SomeResource resource;

    @Before
    public void setUp() throws Exception {
        resource = new SomeResource();
    }

    @Test
    public void should_return_something() throws Exception {
        String result = resource.getIt();

        assertThat("Success").isEqualTo(result);
    }
}